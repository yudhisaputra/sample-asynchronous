package rebelworks.co.service;

import java.util.concurrent.CompletableFuture;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import rebelworks.co.model.User;

/**
 *
 * @author yudhi.saputra@rebelworks.co
 */
@Service
public class UserService {
    private final Logger logger = LoggerFactory.getLogger(UserService.class);
    private final RestTemplate restTemplate;
    
    public UserService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }
    
    @Async
    public CompletableFuture<User> find(String user){
        logger.info("User >>> "+user);
        
        String url = String.format("https://api.github.com/users/%s", user) ;
        User u = restTemplate.getForObject(url, User.class);
        
        try {
            Thread.sleep(1000L);
        } catch (InterruptedException interruptedException) {
             logger.error("Thread Error >>> "+interruptedException);
        }
        
        return CompletableFuture.completedFuture(u);
    }
}