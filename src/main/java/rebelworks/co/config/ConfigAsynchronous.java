package rebelworks.co.config;

import java.util.concurrent.Executor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 *
 * @author yudhi.saputra@rebelworks.co
 */
@EnableAsync
public class ConfigAsynchronous {
    
    public Executor executor(){
        ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
        threadPoolTaskExecutor.setCorePoolSize(0);
        threadPoolTaskExecutor.setMaxPoolSize(0);
        threadPoolTaskExecutor.setQueueCapacity(0);
        threadPoolTaskExecutor.setThreadNamePrefix("JDAsync-");
        threadPoolTaskExecutor.initialize();
        
        return threadPoolTaskExecutor;
    }
}