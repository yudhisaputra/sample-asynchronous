package rebelworks.co;

import java.util.concurrent.CompletableFuture;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import rebelworks.co.model.User;
import rebelworks.co.service.UserService;

/**
 *
 * @author yudhi.saputra@rebelworks.co
 */
@SpringBootApplication
public class Main implements CommandLineRunner{
    
    private final Logger logger = LoggerFactory.getLogger(Main.class);
    private final UserService userService;

    public Main(UserService service) {
        this.userService = service;
    }

    @Override
    public void run(String... args) throws Exception {
        // Start the clock
        long start = System.currentTimeMillis();

        // Kick of multiple, asynchronous lookups
        CompletableFuture<User> userFindOne = userService.find("PivotalSoftware");
        CompletableFuture<User> userFindTwo = userService.find("CloudFoundry");
        CompletableFuture<User> userFindTree = userService.find("Spring-Projects");

        // Wait until they are all done
        CompletableFuture.allOf(userFindOne, userFindTwo, userFindTree).join();

        // Print results, including elapsed time
        logger.info("Time >>> "+(System.currentTimeMillis() - start));
        logger.info("userFindOne >>> "+userFindOne.get());
        logger.info("userFindTwo >>> "+userFindTwo.get());
        logger.info("userFindTree >>> "+userFindTree.get());
    }
    
    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }
}