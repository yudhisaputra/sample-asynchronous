package rebelworks.co.web;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author yudhi.saputra@rebelworks.co
 */
@RestController
public class IndexRestController {
 
    public IndexRestController(){
        
    }
    
    @RequestMapping("/")
    public ResponseEntity<String> index(){
        String message ="Spring Boot it's Running!";
        return new ResponseEntity(message, HttpStatus.OK);
    }
}